import sys

import peewee
import app.constants as _const

from app.country import Country
from app.player import Player
from app.position import Position
from app.team import Team

db = peewee.SqliteDatabase(_const.database['host'], pragmas=_const.database['pragmas'])
db.connect()

models = [
    Country,
    Position,
    Player,
    Player.positions.get_through_model(),
    Team
]

def create_tables():
    db.create_tables(models, safe=True)
    return True

def drop_tables():
    db.drop_tables(models, safe=True)
    return True

def populate_basic_data():
    populate_positions()

def populate_positions():
    positions = []
    for key in _const.POSITION.keys():
        positions.append({"id": _const.POSITION[key], "code": key})

    with db.atomic():
        for idx in range(0, len(positions), 1000):
            Position.insert_many(positions[idx:idx+1000]).execute()



####################################
##### The command
###################################
if __name__ == "__main__":
    arg = sys.argv[1] if len(sys.argv) > 1 else None
    if arg == "create":
        create_tables()
    elif arg == "drop":
        drop_tables()
    elif arg == "reset":
        drop_tables()
        create_tables()
        populate_basic_data()
    else:
        print """
            usage: %s <option>
            Options:
                create - Create all tables
                drop   - Drop all tables
                reset  - Drop and Create all tables
            """
