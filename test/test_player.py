import datetime
import unittest

from app.player import Player

class TestPlayer(unittest.TestCase):
    def setUp(self):
        player = Player()
        player.condition = 80
        player.speed = 5
        player.speed_trait = -2

        self.player = player


    def test_age_attribute(self):
        self.player.birth_date = datetime.date(1987, 5, 23)
        time_dt = datetime.date.today() - self.player.birth_date
        age = time_dt.days / 365

        self.assertEqual(self.player.age, age)


    def test_real_stat(self):
        real_value = self.player.speed + self.player.speed_trait
        real_value *= 1.0 - (self.player.condition / 100.0)

        self.assertEqual(self.player.real_stat('speed'), real_value)
