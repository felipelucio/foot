import unittest

from app.team import Team
from app.player import Player
from app.teamtactic import TeamTactic

class Test_Team(unittest.TestCase):
    def setUp(self):
        self.tactic = TeamTactic()
        # 4-4-2
        self.tactic.set_slot_coord(0, (50, 5))
        self.tactic.set_slot_coord(1, (15, 10))
        self.tactic.set_slot_coord(2, (40, 10))
        self.tactic.set_slot_coord(3, (60, 10))
        self.tactic.set_slot_coord(4, (80, 10))
        self.tactic.set_slot_coord(5, (15, 40))
        self.tactic.set_slot_coord(6, (40, 40))
        self.tactic.set_slot_coord(7, (60, 40))
        self.tactic.set_slot_coord(8, (80, 40))
        self.tactic.set_slot_coord(9, (40, 60))
        self.tactic.set_slot_coord(10, (60, 60))

        self.players = [Player() for i in range(0,22)]

    
