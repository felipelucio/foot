import unittest

import app.constants as _const
from app.teamtactic import TeamTactic
from app.player import Player


class Test_TeamTactic(unittest.TestCase):

    def setUp(self):
        self.tactic = TeamTactic()

        self.tactic.panel_width = 100
        self.tactic.panel_height = 400

        self.player = Player()
        self.tactic.set_slot(0, self.player)

    def test_set_slot_coord(self):
        self.tactic.set_slot_coord(0, (0, 50))
        self.assertEqual(self.tactic.slots_coord[0], (0, 50))

    def test_set_slot(self):
        player = Player()
        self.tactic.set_slot(1, player)
        self.assertIs(self.tactic.slots[1], player)

    def test_set_slot_with_repeated_player_cleared_previous_slot(self):
        self.tactic.set_slot(1, self.player)
        self.assertIsNot(self.tactic.slots[0], self.player)

    def test_set_slot_with_repeated_player(self):
        self.tactic.set_slot(1, self.player)
        self.assertIs(self.tactic.slots[1], self.player)


    def test_pixels_to_internal_coord(self):
        x = 50
        y = 350

        test_internal_x = (x * 100) / self.tactic.panel_width
        test_internal_y = (y * 100) / self.tactic.panel_height

        test_internal_pos = (test_internal_x, test_internal_y)

        internal_pos = self.tactic.pixels_to_internal_coord(x, y)
        self.assertEqual(internal_pos, test_internal_pos)

    def test_internal_coord_to_position(self):
        # GK
        x, y = (50, 5)
        gk_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['GK'], gk_pos, 'Got: %s, expected %s' % (gk_pos, _const.POSITION['GK']))

        # Defender
        x, y = (50, 18)
        d_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['D'], d_pos, 'Got: %s, expected %s' % (d_pos, _const.POSITION['D']))

        # L Defender
        x, y = (10, 10)
        ld_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['LD'], ld_pos, 'Got: %s, expected %s' % (ld_pos, _const.POSITION['LD']))

        # R Defender
        x, y = (80, 10)
        rd_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['RD'], rd_pos, 'Got: %s, expected %s' % (rd_pos, _const.POSITION['RD']))

        # Libero
        x, y = (50, 10)
        l_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['L'], l_pos, 'Got: %s, expected %s' % (l_pos, _const.POSITION['L']))

        # Mid Defender
        x, y = (50, 25)
        md_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['MD'], md_pos, 'Got: %s, expected %s' % (md_pos, _const.POSITION['MD']))

        # L Mid Defender
        x, y = (10, 30)
        lmd_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['LMD'], lmd_pos, 'Got: %s, expected %s' % (lmd_pos, _const.POSITION['LMD']))

        # R Mid Defender
        x, y = (80, 30)
        rmd_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['RMD'], rmd_pos, 'Got: %s, expected %s' % (rmd_pos, _const.POSITION['RMD']))

        # Middle
        x, y = (50, 50)
        m_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['M'], m_pos, 'Got: %s, expected %s' % (m_pos, _const.POSITION['M']))

        # L Middle
        x, y = (10, 50)
        lm_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['LM'], lm_pos, 'Got: %s, expected %s' % (lm_pos, _const.POSITION['LM']))

        # R Middle
        x, y = (80, 50)
        rm_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['RM'], rm_pos, 'Got: %s, expected %s' % (rm_pos, _const.POSITION['RM']))

        # Offensive Middle
        x, y = (50, 70)
        om_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['OM'], om_pos, 'Got: %s, expected %s' % (om_pos, _const.POSITION['OM']))

        # L Winger
        x, y = (10, 70)
        lw_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['LW'], lw_pos, 'Got: %s, expected %s' % (lw_pos, _const.POSITION['LW']))

        # R Winger
        x, y = (80, 70)
        rw_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['RW'], rw_pos, 'Got: %s, expected %s' % (rw_pos, _const.POSITION['RW']))

        # Striker
        x, y = (50, 85)
        s_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['S'], s_pos, 'Got: %s, expected %s' % (s_pos, _const.POSITION['S']))

        # Withdraw Striker
        x, y = (50, 80)
        s_pos = self.tactic.internal_coord_to_position(x, y)
        self.assertIn(_const.POSITION['WS'], s_pos, 'Got: %s, expected %s' % (s_pos, _const.POSITION['WS']))
