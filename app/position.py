from peewee import CharField

from app.basemodel import BaseModel

class Position(BaseModel):
    code = CharField()
