POSITION = {
    'GK': 1, # Goal keeper
    'D': 2, # Defender
    'LD': 3, # Right Defender
    'RD': 4, # Left Defender
    'L' : 5, # Libero
    'MD': 6, # Central Middle Defender
    'LMD': 7, # Left Middle Defender
    'RMD': 8, # Right Middle Defender
    'M': 9, # Middle
    'LM': 10, # Left Middle
    'RM': 11, # Right Middle
    'LW': 12, # Left Winger
    'RW': 13, # Right Winger
    'OM': 14, # Offensive Middle
    'S': 15, # Striker
    'WS': 16, # Withdraw Striker
}

POSITION_NAME = {
    1: "Goal Keeper",
    2: "Central Defender",
    3: "Left Defender",
    4: "Right Defender",
    5: "Libero",
    6: "Central Middle Defender",
    7: "Left Middle Defender",
    8: "Right Middle Defender",
    9: "Central Midfielder",
    10: "Left Midfielder",
    11: "Right Midfielder",
    12: "Left Winger",
    13: "Right Winger",
    14: "Offensive Midfielder",
    15: "Striker",
    16: "Withdraw Striker"
}

database = {
    "host": "db/db.sqlite",
    "pragmas": ()
}
