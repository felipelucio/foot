from peewee import CharField, FixedCharField

from app.basemodel import BaseModel

class Country(BaseModel):
    name = CharField()
    code = FixedCharField(max_length=2)
