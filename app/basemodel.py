import peewee

import app.constants as _const

db = peewee.SqliteDatabase(_const.database["host"], pragmas=_const.database["pragmas"])

class BaseModel(peewee.Model):
    class Meta:
        database = db
