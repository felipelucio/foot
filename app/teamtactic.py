import app.constants as _const
from app.basemodel import BaseModel

class TeamTactic(BaseModel):
    def __init__(self):
        self.panel_width = 0
        self.panel_height = 0

        self.slots = { i:None for i in range(0, 22) }
        self.slots_coord = { i:None for i in range(0, 11) }


    def set_slot_coord(self, slot, coord):
        if slot < 0 or slot > 10:
            raise IndexError('Index out of boundaries: 0-10')

        self.slots_coord[slot] = coord


    def set_slot(self, slot, player):
        if slot < 0 or slot > 21:
            raise IndexError('Index out of boundaries: 0-21')

        new_slots = { i:None for i in range(0, 22) }
        for i in self.slots:
            if self.slots[i] is not player:
                new_slots[i] = self.slots[i]
            else:
                new_slots[i] = None

        self.slots = new_slots
        self.slots[slot] = player


    def set_panel_size(self, w, h):
        self.panel_width = w
        self.panel_height = h


    def pixels_to_internal_coord(self, pos_x, pos_y):
        internal_x = (pos_x * 100) / self.panel_width
        internal_y = (pos_y * 100) / self.panel_height

        return (internal_x, internal_y)

    def internal_coord_to_position(self, x, y):
        positions = []

        # GK
        if (x >= 40 and x < 60) and (y >= 0 and y < 10):
            positions.append(_const.POSITION['GK'])

        # Defender
        if (x >= 15 and x < 75) and (y >= 0 and y < 20):
            positions.append(_const.POSITION['D'])

        # L Defender
        if (x >= 0 and x < 15) and (y >= 0 and y < 20):
            positions.append(_const.POSITION['LD'])

        # R Defender
        if (x >= 75 and x < 100) and (y >= 0 and y < 20):
            positions.append(_const.POSITION['RD'])

        # Libero
        if (x >= 15 and x < 75) and (y >= 10 and y < 15):
            positions.append(_const.POSITION['L'])

        # Mid Defender
        if (x >= 15 and x < 75) and (y >= 20 and y < 40):
            positions.append(_const.POSITION['MD'])

        # L Mid Defender
        if (x >= 0 and x < 15) and (y >= 20 and y < 40):
            positions.append(_const.POSITION['LMD'])

        # R Mid Defender
        if (x >= 75 and x < 100) and (y >= 20 and y < 40):
            positions.append(_const.POSITION['RMD'])

        # Middle
        if (x >= 15 and x < 75) and (y >= 40 and y < 60):
            positions.append(_const.POSITION['M'])

        # L Middle
        if (x >= 0 and x < 15) and (y >= 40 and y < 60):
            positions.append(_const.POSITION['LM'])

        # R Middle
        if (x >= 75 and x < 100) and (y >= 40 and y < 60):
            positions.append(_const.POSITION['RM'])

        # Offensive Middle
        if (x >= 15 and x < 75) and (y >= 60 and y < 80):
            positions.append(_const.POSITION['OM'])

        # L Winger
        if (x >= 0 and x < 15) and (y >= 60 and y < 100):
            positions.append(_const.POSITION['LW'])

        # R Winger
        if (x >= 75 and x < 100) and (y >= 60 and y < 100):
            positions.append(_const.POSITION['RW'])

        # Striker and Withdraw Striker
        if (x >= 15 and x < 75) and (y >= 80 and y < 100):
            positions.append(_const.POSITION['S'])
            positions.append(_const.POSITION['WS'])

        return positions
