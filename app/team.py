from peewee import CharField
from app.basemodel import BaseModel

class Team(BaseModel):
    name = CharField()
