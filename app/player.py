import datetime

from peewee import *
from playhouse.fields import ManyToManyField

from app.basemodel import BaseModel
from app.country import Country
from app.position import Position
from app.team import Team

class Player(BaseModel):
    first_name = CharField()
    last_name = IntegerField()
    nick_name = IntegerField()
    birth_date = DateField()

    country = ForeignKeyField(Country)
    team = ForeignKeyField(Team)
    positions = ManyToManyField(Position)

    height = IntegerField()
    condition = IntegerField()

    # hidden attributes
    adaptability = IntegerField()
    dirtiness = IntegerField()
    injury_proneness = IntegerField()
    versatility = IntegerField()

    # technical
    ability = IntegerField()
    ability_trait = IntegerField()
    crossing = IntegerField()
    crossing_trait = IntegerField()
    dribbling = IntegerField()
    dribbling_trait = IntegerField()
    finishing = IntegerField()
    finishing_trait = IntegerField()
    free_kicking = IntegerField()
    free_kicking_trait = IntegerField()
    heading = IntegerField()
    heading_trait = IntegerField()
    long_shooting = IntegerField()
    long_shooting_trait = IntegerField()
    marking = IntegerField()
    marking_trait = IntegerField()
    passing = IntegerField()
    passing_trait = IntegerField()
    tackling = IntegerField()
    tackling_trait = IntegerField()

    # GK
    handling = IntegerField()
    handling_trait = IntegerField()
    reflexes = IntegerField()
    reflexes_trait = IntegerField()
    throwing = IntegerField()
    throwing_trait = IntegerField()

    # mental
    anticipation = IntegerField()
    anticipation_trait = IntegerField()
    bravery = IntegerField()
    bravery_trait = IntegerField()
    concentration = IntegerField()
    concentration_trait = IntegerField()
    determination = IntegerField()
    determination_trait = IntegerField()
    improvisation = IntegerField()
    improvisation_trait = IntegerField()
    leadership = IntegerField()
    leadership_trait = IntegerField()
    positioning = IntegerField()
    positioning_trait = IntegerField()
    teamwork = IntegerField()
    teamwork_trait = IntegerField()
    vision = IntegerField()
    vision_trait = IntegerField()

    # physical
    acceleration = IntegerField()
    acceleration_trait = IntegerField()
    agility = IntegerField()
    agility_trait = IntegerField()
    jumping = IntegerField()
    jumping_trait = IntegerField()
    speed = IntegerField()
    speed_trait = IntegerField()
    stamina = IntegerField()
    stamina_trait = IntegerField()
    strength = IntegerField()
    strength_trait = IntegerField()

    @property
    def age(self):
        time_dt = datetime.date.today() - self.birth_date
        return (time_dt.days / 365)


    def real_stat(self, stat):
        base_value = getattr(self, stat)
        trait_value = getattr(self, '_'.join([stat, 'trait']))

        real_value = base_value + trait_value
        real_value *= 1.0 - (self.condition / 100.0)

        return real_value
